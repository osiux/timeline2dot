#!/bin/bash

# This script comes with ABSOLUTELY NO WARRANTY, use at own risk
# Copyright (C) 2022 Osiris Alejandro Gomez <osiux@osiux.com>
# Copyright (C) 2022 Osiris Alejandro Gomez <osiris@gcoop.coop>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

[[ -n "$GRAPH_BGCOLOR"        ]] || GRAPH_BGCOLOR='#000000'
[[ -n "$GRAPH_RESOLUTION"     ]] || GRAPH_RESOLUTION='72'
[[ -n "$GRAPH_NODESEP"        ]] || GRAPH_NODESEP='0.8'
[[ -n "$GRAPH_RANKSEP"        ]] || GRAPH_RANKSEP='0.5'

[[ -n "$NODE_FONTNAME"        ]] || NODE_FONTNAME='InconsolataBold'
[[ -n "$NODE_FONTSIZE"        ]] || NODE_FONTSIZE='14'
[[ -n "$NODE_FONTCOLOR"       ]] || NODE_FONTCOLOR='#cfff00'
[[ -n "$NODE_COLOR"           ]] || NODE_COLOR='#9fc800'

[[ -n "$EDGE_FONTNAME"        ]] || EDGE_FONTNAME='Inconsolata'
[[ -n "$EDGE_FONTSIZE"        ]] || EDGE_FONTSIZE='13'
[[ -n "$EDGE_FONTCOLOR"       ]] || EDGE_FONTCOLOR='#ffff00'
[[ -n "$EDGE_COLOR"           ]] || EDGE_COLOR='#80c800'

[[ -n "$TAG_SHAPE"            ]] || TAG_SHAPE='rect'
[[ -n "$TAG_EDGE_COLOR"       ]] || TAG_EDGE_COLOR="$NODE_COLOR"
[[ -n "$TAG_SHAPE_STYLE"      ]] || TAG_SHAPE_STYLE='filled'
[[ -n "$TAG_SHAPE_FILL_COLOR" ]] || TAG_SHAPE_FILL_COLOR='#9fc800'
[[ -n "$TAG_SHAPE_FONTCOLOR"  ]] || TAG_SHAPE_FONTCOLOR='#000000'

[[ -n "$TAG_EDGE_LABEL"       ]] || TAG_EDGE_LABEL='█'
[[ -n "$DAY_EDGE_LABEL"       ]] || DAY_EDGE_LABEL='|'

[[ -n "$START"            ]] || START="$(date -d "now - 30 days" +'%F')"
[[ -n "$DAYS"             ]] || DAYS="30"
[[ -n "$REGEX_VERSION"    ]] || REGEX_VERSION="v[0-9]+\.[0-9]+\.[0-9]+"
[[ -n "$TIMELINE"         ]] || TIMELINE='timeline.txt'
[[ -z "$1"                ]] || TIMELINE="$1"
[[ -e "$TIMELINE"         ]] || exit 1

DAY_TAG="$(mktemp)"

DOT_HEAD=$(cat << EOF
digraph timeline2dot {

  rankdir=TB;
  graph [truecolor=true bgcolor="$GRAPH_BGCOLOR"]
  resolution=$GRAPH_RESOLUTION;
  nodesep=$GRAPH_NODESEP;
  ranksep=$GRAPH_RANKSEP;
  node [shape="plain", fontname="$NODE_FONTNAME", fontsize="$NODE_FONTSIZE", fontcolor="$NODE_FONTCOLOR", color="$NODE_COLOR"];
  edge [arrowhead="none",arrowtail="none",fontname="$EDGE_FONTNAME", fontsize="$EDGE_FONTSIZE", fontcolor="$EDGE_FONTCOLOR", color="$EDGE_COLOR", style="filled"];
EOF
)

# GET_TAG_DAYS
for D in $(seq 0 365)
do

  DAY="$(date -d "$START + $D DAYS" +%F)"
  LINE="$(grep -E "^$DAY .*$REGEX_VERSION" "$TIMELINE")"
  [[ -z "$LINE" ]] && continue
  VERSION="$(echo "$LINE" | grep -Eo "$REGEX_VERSION" | head -1)"
  TAG="$(echo "$LINE" | awk '{print $1,$3}')/$VERSION"
  echo "$TAG" | grep -E "$REGEX_VERSION"

done > "$DAY_TAG"


printf "%s\n\n" "$DOT_HEAD"

# TAG_DAYS_RANK
printf "{rank=same\n"
for D in $(seq 0 "$DAYS")
do
  MOD="$(date -d "$START + $D DAYS" +%b%d)"
  DAY="$(date -d "$START + $D DAYS" +%F)"
  grep -E "^$DAY" "$DAY_TAG" >/dev/null && printf "%s_tag;\n" "$MOD"
done
printf "}\n\n"

# ALL_DAYS_RANK
printf "{rank=same\n"
for D in $(seq 0 "$DAYS")
do
  MOD="$(date -d "$START + $D DAYS" +%b%d)"
  printf "%s_day;\n" "$MOD"
done
printf "}\n\n"

# NODES_AND_TAG_RELATIONSHIPS
for D in $(seq 0 "$DAYS")
do
  DAY="$(date -d "$START + $D DAYS" +%F)"
  MOD="$(date -d "$START + $D DAYS" +%b%d)"
  TAG="${MOD}_tag"
  TXT="$(grep -E "^$DAY" "$DAY_TAG" | cut -d ' ' -f2-)"

  if [[ -z "$TXT" ]]
  then
    printf "%s_day [label=\"%s\"];\n" "$MOD" "$DAY_EDGE_LABEL"
  else
    printf "%s_day [label=\"%s\"];\n" "$MOD" "$TAG_EDGE_LABEL"
    printf "%s_tag [label=\"%s\", shape=\"%s\", style=\"%s\", fillcolor=\"%s\", fontcolor=\"%s\"];\n" \
      "$MOD" "$TXT" "$TAG_SHAPE" "$TAG_SHAPE_STYLE" "$TAG_SHAPE_FILL_COLOR" "$TAG_SHAPE_FONTCOLOR"
    printf "%s_tag -> %s_day [label=\" %s\", color=\"%s\"];\n" \
      "$MOD" "$MOD" "$DAY" "$TAG_EDGE_COLOR"
  fi

done

printf "\n\n"

# ALL_DAYS_RELATIONSHIPS
for D in $(seq 0 "$DAYS")
do
  MOD="$(date -d "$START + $D DAYS" +%b%d)"
  printf "%s_day -> " "$MOD"
done | sed 's/ -> $/;/g'

# DOT_FOOTER
printf "\n\n%s\n" "}"

rm -f "$DAY_TAG"
