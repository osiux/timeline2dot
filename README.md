# `timeline2dot`

## Overview

Read a `.txt` file and generate a `.dot` file with timeline graph.

## dependencies

The main dependency is `graphviz`, install with:

```bash

apt install graphviz

```

Other utils tools:

```bash

apt-get make qiv

```

## Execute

```bash

START=2022-08-10 DAYS=10 timeline2dot [timeline.txt]

```

## Example

### `timeline.txt`

```
2022-08-04 dev: gitlab-bash-utils v0.1.0 first public version
2022-08-04 dev: gitlab-bash-utils v0.2.0 fix dont ignore title and description in mr new
2022-08-13 dev: timeline2dot v0.1.0 first release
```

### `timeline.png`

![timeline](timeline.png)

## License

GNU General Public License, GPLv3.

## Author Information

This repo was created in 2022 by
 [Osiris Alejandro Gomez](https://osiux.com/), worker cooperative of
 [gcoop Cooperativa de Software Libre](https://www.gcoop.coop/).
