2019-10-03 dev: ansible_tools v0.1.0 Add git-ls-remote-tags and requirements-version-update
2019-10-03 dev: ansible_tools v0.1.1 Add README.md and example of requirements.yml
2020-01-30 dev: ansible_tools v0.1.2 Better tags sort
2020-06-14 dev: ansible_tools v0.1.3 add git-ls-remote-heads
2020-06-14 dev: ansible_tools v0.1.4 update README.md
2020-09-20 dev: ansible_tools v0.1.5 add requirements-ssh2https
2020-09-20 dev: ansible_tools v0.1.6 requirements-ssh2https: use requirements.yml in roles or in tests/roles
2020-11-09 dev: txt-bash-jrnl v0.1.0 first release
2020-12-16 dev: ansible_tools v0.1.7 general refactor, add support for help
2021-05-05 dev: ansible_tools v0.1.8 general refactor, add new scripts, update and fix many scripts
2021-06-09 dev: direplos v0.1.0 first release
2021-12-31 dev: ansible_tools v0.2.0 happy gnu year with new scripts and lots of fixed scripts
2022-08-04 dev: gitlab-bash-utils v0.1.0 first public version
2022-08-04 dev: gitlab-bash-utils v0.2.0 fix dont ignore title and description in mr new
2022-08-13 dev: timeline2dot v0.1.0 first release
